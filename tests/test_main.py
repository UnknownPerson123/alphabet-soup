import io
import logging
import os
import sys
import unittest
import unittest.mock

from alphabetsoup import word_search

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
test_log = logging.getLogger("TestLog")

input_simple_filepath = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "./files/input-simple.txt")
)
input_complex_filepath = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "./files/input-complex.txt")
)

output_simple_filepath = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "./files/output-simple.txt")
)
output_complex_filepath = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "./files/output-complex.txt")
)

test_words_dict = {
    "one": {"length": 3, "no_spaces_backward": "eno", "no_spaces_forward": "one"},
    "three": {"length": 5, "no_spaces_backward": "eerht", "no_spaces_forward": "three"},
    "two": {"length": 3, "no_spaces_backward": "owt", "no_spaces_forward": "two"},
}
test_locations_map = {"eerht": [((4, 4), (0, 0))], "one": [((1, 4), (4, 4))]}
test_locations_map_2 = {
    "AB": [((0, 0), (0, 1))],
    "AD": [((0, 0), (1, 0))],
    "AE": [((0, 0), (1, 1))],
    "BC": [((0, 1), (0, 2))],
    "BE": [((0, 1), (1, 1))],
    "BF": [((0, 1), (1, 2))],
    "CF": [((0, 2), (1, 2))],
    "DB": [((1, 0), (0, 1))],
    "DE": [((1, 0), (1, 1))],
    "DH": [((1, 0), (2, 0))],
    "DI": [((1, 0), (2, 1))],
    "EC": [((1, 1), (0, 2))],
    "EF": [((1, 1), (1, 2))],
    "EI": [((1, 1), (2, 1))],
    "EJ": [((1, 1), (2, 2))],
    "FJ": [((1, 2), (2, 2))],
    "HE": [((2, 0), (1, 1))],
    "HI": [((2, 0), (2, 1))],
    "IF": [((2, 1), (1, 2))],
    "IJ": [((2, 1), (2, 2))],
}

test_print_word_locations_result = "one 1:4 4:4\nthree 0:0 4:4\ntwo"

test_puzzle_matrix = [["A", "B", "C"], ["D", "E", "F"], ["H", "I", "J"]]


class TestAlphabetSoup(unittest.TestCase):
    def setUp(self):
        self.word_search_parser = word_search.WordSearchParser()

    #######################
    #### Test Functions ###
    #######################

    @unittest.mock.patch("sys.stdout", new_callable=io.StringIO)
    def test_process_simple(self, mock_stdout):
        self._test_process(input_simple_filepath, output_simple_filepath, mock_stdout)

    @unittest.mock.patch("sys.stdout", new_callable=io.StringIO)
    def test_process_complex(self, mock_stdout):
        self._test_process(input_complex_filepath, output_complex_filepath, mock_stdout)

    # parse_args(self, argv)
    def test_parse_args_empty_argv(self):
        with self.assertRaises(SystemExit):
            self.word_search_parser.parse_args([])

    def test_parse_args_single_argv(self):
        self.assertEqual(self.word_search_parser.parse_args(["test"]), "test")

    def test_parse_args_too_many_argv(self):
        with self.assertRaises(SystemExit):
            self.word_search_parser.parse_args(["test", "test"])

    # parse_size(self, dimension_string)
    def test_parse_size_valid(self):
        self.assertEqual(self.word_search_parser.parse_size("5x5"), [5, 5])

    def test_parse_size_too_few(self):
        with self.assertRaises(Exception):
            self.word_search_parser.parse_size("5")

    def test_parse_size_too_many(self):
        with self.assertRaises(Exception):
            self.word_search_parser.parse_size("5x5x5")

    # build_puzzle_matrix(self, row_strings)
    def test_puzzle_matrix_valid(self):
        self.assertEqual(
            # Note the extra space after the three (this validates both strings with and without trailing spaces)
            self.word_search_parser.build_puzzle_matrix(["A B C ", "D E F", "H I J"]),
            test_puzzle_matrix,
        )

    # process_words(self, word_list)
    def test_process_words_valid(self):
        words_dict, lengths = self.word_search_parser.process_words(
            ["one", "two", "three"]
        )
        # Verify the dictionary returns the expected information
        self.assertEqual(words_dict, test_words_dict)
        # Verify the keys remained in the same order
        # Enhancement opportunity: Add more tests for different orders (to ensure this isn't a fluke)
        self.assertEqual(list(words_dict.keys()), ["one", "two", "three"])
        # Verify the set of lengths is appropriate (no duplicates included)
        self.assertEqual(lengths, {3, 5})

    # build_location_map(self, puzzle_matrix, num_rows, num_columns, word_lengths)
    # Enhancement opportunity: Expand testing in conjunction with refactoring this function
    def test_build_location_map_valid(self):
        self.assertEqual(
            self.word_search_parser.build_location_map(test_puzzle_matrix, 3, 3, {2}),
            test_locations_map_2,
        )

    # print_word_locations(self, words_info, locations_map)
    @unittest.mock.patch("sys.stdout", new_callable=io.StringIO)
    def test_print_word_locations_valid(self, mock_stdout):
        self.word_search_parser.print_word_locations(
            test_words_dict, test_locations_map
        )
        self.assertEqual(
            mock_stdout.getvalue().strip(), test_print_word_locations_result
        )

    # _update_location_map(self, word_location_map, word, location)
    def test__update_location_map_valid(self):
        test_map = {"one": [((0, 0), (1, 1))]}
        self.word_search_parser._update_location_map(test_map, "two", ((2, 2), (3, 3)))
        new_word_locations = test_map.get("two")
        self.assertEqual(new_word_locations, [((2, 2), (3, 3))])

        old_word_location = test_map.get("one")
        self.word_search_parser._update_location_map(test_map, "one", ((4, 4), (5, 5)))
        self.assertEqual(old_word_location, [((0, 0), (1, 1)), ((4, 4), (5, 5))])

    # _lr_translate(self, num_columns, col_num, num_to_adjust=None)
    def test__lr_translate_valid(self):
        # Extreme Right to Left
        self.assertEqual(self.word_search_parser._lr_translate(5, 4), 0)

        # Extreme Left to Right
        self.assertEqual(self.word_search_parser._lr_translate(5, 0), 4)

        # Mid Right to Left
        self.assertEqual(self.word_search_parser._lr_translate(6, 4), 1)

        # Mid Left to Right
        self.assertEqual(self.word_search_parser._lr_translate(6, 2), 3)

        # Middle Column (odd num)
        self.assertEqual(self.word_search_parser._lr_translate(5, 2), 2)

    ##########################
    #### Helper Functions ####
    ##########################

    def _test_process(self, input_filepath, output_filepath, mock_stdout):
        """
        Integration level test, given a file verify output
        """
        self.word_search_parser.process([input_filepath])
        # test_log.debug(mock_stdout.getvalue())
        with io.open(output_filepath, "r") as output_file:
            output_string = output_file.read()
            # test_log.debug(output_string)

            self.assertEqual(mock_stdout.getvalue().strip(), output_string.strip())


if __name__ == "__main__":
    unittest.main()
