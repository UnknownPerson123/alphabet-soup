# -*- coding: utf-8 -*-
"""

This module defines the funtionality necessary to solve a word search.
It provides both a class, which can be imported elsewhere, as well as
the main function so it can be called directly.

Example:
    $ python word_search.py ../tests/files/input-simple.txt

Todo:
    * Enhance error handling (expand use, create custom errors, etc.)
    * Add a divider between locations if multiples are found
    * Add the ability to filter out "sub" words
        Example: "Candy Cane" should not match "Candy" (assuming both are in word list)
                 The word "Candy" should only be found once.
    * Generalize "print_word_locations" to allow customized outputs (to file, etc.)
    * Clean up "build_location_map" to require fewer parameters and make lengths optional

"""

import sys
import argparse
import numpy


class WordSearchParser:
    """Parser class which handles all parts of solving a word search.

    This class assumes the following file format for the word search:

        "The file contains three parts. The first part is the first line, and specifies
        the number of rows and columns in the grid of characters, separated by an 'x'.
        The second part provides the grid of characters in the word search. The third
        part in the file specifies the words to be found.

        The first line indicates how many following lines in the file contain the rows
        of characters that make up the word search grid. Each row in the word search
        grid will have the specified number of columns of characters, each separated
        with a space. The remaining lines in the file specify the words to be found."

        Example:
        3x3
        A B C
        D E F
        G H I
        ABC
        AEI

    Arguments:
        file_path (str): Path to the word search input file.

    """

    def process(self, argv):
        """The 'workhorse' function that coordinates the solution from file to answer

        Args:
            argv: The arguments to be parsed.

        Returns:
            No return (though it does print to stdout).

        """
        file_path = self.parse_args(argv)
        with open(file_path, "r") as file:
            num_rows, num_columns = self.parse_size(file.readline())

            row_strings = [next(file) for x in range(num_rows)]
            puzzle_matrix = self.build_puzzle_matrix(row_strings)

            words = [word.strip() for word in [line for line in file]]
            words_info, word_lengths = self.process_words(words)

            location_map = self.build_location_map(
                puzzle_matrix, num_rows, num_columns, word_lengths
            )
            self.print_word_locations(words_info, location_map)

    def parse_args(self, argv):
        """Parses the arguments given for "file_path"

        Args:
            argv: The arguments to parse.

        Returns:
            The string representation of the "file_path" argument.

        """
        parser = argparse.ArgumentParser()
        parser.add_argument("file_path", type=str, help="File path to parse and search")
        return parser.parse_args(argv).file_path

    def parse_size(self, dimension_string):
        """Parses a string representation of two dimensions

        Args:
            dimension_string: The dimensions in the exact format "<int>x<int>"

        Returns:
            [int, int]

        """
        dimensions = dimension_string.split("x")
        if len(dimensions) != 2:
            raise Exception(
                "Invalid dimension string. First line must be '<Column Count>x<Row Count>'."
            )
        return [int(dimension.strip()) for dimension in dimensions]

    def build_puzzle_matrix(self, row_strings):
        """Reformats row strings into a matrix (splits on space)

        Note:
            This function strips trailing white space from the individual elements

        Args:
            row_strings: List of strings. Each string should represent a row of the matrix.

        Returns:
            [[String]] - Two dimensional array of strings

        """
        puzzle_matrix = []
        for row in row_strings:
            puzzle_matrix.append([item.strip() for item in row.strip().split(" ")])
        return puzzle_matrix

    def process_words(self, word_list):
        """Builds a dictionary of information for each word

        Note:
            Each dictionary will be in the format:
            {
                "length": <int>                # The length of the word,
                "no_spaces_formard": <string>  # The word with all spaces removed
                "no_spaces_backward": <string> # The word with all spaces removed and the characters reversed
            }

        Args:
            word_list: List of strings (words) to process.

        Returns:
            {Word: Information}, set(Word Lengths)

        """
        lengths = set()
        words_dict = {}

        for word in word_list:
            word_no_spaces = word.replace(" ", "")
            word_length = len(word_no_spaces)
            word_dict = {
                "length": word_length,
                "no_spaces_forward": word_no_spaces,
                "no_spaces_backward": word_no_spaces[::-1],
            }

            lengths.add(
                word_length
            )  # Dict's are ordered in py3, so this meets the order requirement
            words_dict[word] = word_dict

        return words_dict, lengths

    def build_location_map(self, puzzle_matrix, num_rows, num_columns, word_lengths):
        """This function builds a map of all possible character combinations and their locations

        Note:
            This function attempts to find a balance between time and space. Building this map
            takes time upfront, but we attempt to minimize that by checking the available chars
            in each dimension and only processing those that make sense. We attempt to minimize the
            space necessary by only storing the locations of character sets that match the length
            or a word that will be searched for.

            The "locations" will be a list of nested tuples in the format ((x1,y1),(x2,y2))

        Args:
            puzzle_matrix: The matrix (2D) of characters to map.
            num_rows: The number of rows in the matrix.
            num_columns: The number of columns in the matrix.
            word_lengths: A set of all applicable word lengths.

        Returns:
            Dictionary in the format:
            {
                String: [locations]
            }

        """
        numpy_array = numpy.array(puzzle_matrix)
        flipped_lr_numpy_array = numpy.fliplr(
            numpy_array
        )  # Special for top right/bottom left diagonal
        sorted_lengths = sorted(word_lengths)
        shortest_word = sorted_lengths[0]

        word_location_map = {}

        # Cache the results for each row/column/diagonal so we don't repeat this work
        rows = {}
        columns = {}
        diagonals = {}
        flipped_diagonals = {}

        for row_num in range(num_rows):
            for col_num in range(num_columns):

                # All of these include the current character
                column_chars_remaining = num_columns - col_num
                column_chars_passed = col_num + 1
                row_chars_remaining = num_rows - row_num

                # Both the horizontal and diagonal are handled here since they use the same remaining count
                if shortest_word <= column_chars_remaining:
                    # Attempt to grab the row from the cache variable, otherwise pull it from the array and store it
                    row_array = rows.setdefault(
                        row_num, numpy_array[row_num, :].tolist()
                    )

                    diagonal_position = (
                        col_num - row_num
                    )  # The origin (0,0) defines the 0th diagonal
                    diagonal_array = diagonals.setdefault(
                        diagonal_position,
                        numpy.diag(numpy_array, k=diagonal_position).tolist(),
                    )

                    for length in word_lengths:
                        word_start = (row_num, col_num)
                        zero_adj_length = length - 1

                        horizontal_word = "".join(row_array[col_num : col_num + length])
                        horizontal_word_end = (row_num, (col_num + zero_adj_length))

                        starting_position = row_num if col_num > row_num else col_num

                        diagonal_word = "".join(
                            diagonal_array[
                                starting_position : (starting_position + length)
                            ]
                        )
                        diagonal_word_end = (
                            (row_num + zero_adj_length),
                            (col_num + zero_adj_length),
                        )

                        if len(horizontal_word) == length:
                            self._update_location_map(
                                word_location_map,
                                horizontal_word,
                                (word_start, horizontal_word_end),
                            )

                        if len(diagonal_word) == length:
                            self._update_location_map(
                                word_location_map,
                                diagonal_word,
                                (word_start, diagonal_word_end),
                            )

                        # Short circuit if once the length is longer than the remaining letters
                        if length >= column_chars_remaining:
                            break

                # Since we flip the matrix to facilitate the opposite diagonal, we actually care about the chars we have passed
                if shortest_word <= column_chars_passed:
                    diagonal_position = (
                        col_num - row_num
                    )  # The origin (0,0) defines the 0th diagonal
                    flipped_diagonal_position = self._lr_translate(
                        num_columns, col_num, diagonal_position
                    )
                    flipped_diagonal_array = flipped_diagonals.setdefault(
                        flipped_diagonal_position,
                        numpy.diag(
                            flipped_lr_numpy_array, k=flipped_diagonal_position
                        ).tolist(),
                    )
                    for length in word_lengths:
                        flipped_col_num = self._lr_translate(num_columns, col_num)
                        word_end = (row_num, col_num)
                        zero_adj_length = length - 1

                        starting_position = (
                            row_num if flipped_col_num > row_num else flipped_col_num
                        )

                        # Since the whole array was flipped to scan it, we need to flip the words back again to ensure the correct start/end order
                        flipped_diagonal_word = "".join(
                            flipped_diagonal_array[
                                starting_position : starting_position + length
                            ]
                        )[::-1]
                        flipped_col_start = flipped_col_num + zero_adj_length
                        flipped_col_start = self._lr_translate(
                            num_columns, flipped_col_start
                        )

                        flipped_diagonal_word_start = (
                            row_num + zero_adj_length,
                            flipped_col_start,
                        )

                        if len(flipped_diagonal_word) == length:
                            self._update_location_map(
                                word_location_map,
                                flipped_diagonal_word,
                                (flipped_diagonal_word_start, word_end),
                            )

                        # Short circuit (reversed since we are looking left to right once flipped)
                        if length >= column_chars_passed:
                            break

                if shortest_word <= row_chars_remaining:
                    # Attempt to grab the column from the cache variable, otherwise pull it from the array and store it
                    col_array = columns.setdefault(
                        col_num, numpy_array[:, col_num].tolist()
                    )

                    for length in word_lengths:
                        word_start = (row_num, col_num)
                        zero_adj_length = length - 1

                        vertical_word = "".join(col_array[row_num : row_num + length])
                        vertical_word_end = ((row_num + zero_adj_length), col_num)

                        self._update_location_map(
                            word_location_map,
                            vertical_word,
                            (word_start, vertical_word_end),
                        )

                        # Short circuit if once the length is longer than the remaining letters
                        if length >= row_chars_remaining:
                            break

        return word_location_map

    def print_word_locations(self, words_info, locations_map):
        """Outputs the mapped locations of the words provided

        Args:
            words_info: The dictionary describing each word to search for.
            locations_map: The dictionary linking words to their start/end points.

        Returns:
            No return (it does print).

        """
        # Enhancement opportunity: Add a divider between locations if multiple are found
        for word, data in words_info.items():
            output_string = word
            for location in locations_map.get(data["no_spaces_forward"], list()):
                output_string += f" {location[0][0]}:{location[0][1]} {location[1][0]}:{location[1][1]}"
            for location in locations_map.get(data["no_spaces_backward"], list()):
                # We flip the location because this word was found "backwards"
                output_string += f" {location[1][0]}:{location[1][1]} {location[0][0]}:{location[0][1]}"
            print(output_string)

    def _update_location_map(self, word_location_map, word, location):
        """Facilitates using a dictionary as a "cache"

        Note:
            The dictionary is updated in place.

        Args:
            word_location_map: dictionary to search (and update if necessary).
            word: Key to set if it is doesn't already exist.
            location: Object to append to list for the given key (word)

        Returns:
            No return

        """
        value_to_update = word_location_map.setdefault(word, list())
        value_to_update.append(location)
        word_location_map[word] = value_to_update

    def _lr_translate(self, num_columns, col_num, num_to_adjust=None):
        """Using the median as a pivot, this function flips a number from left to right (or right to left)

        Note:
            If the number IS the median the same number will be returned

        Args:
            num_columns: The total length of the set
            col_num: The current position
            num_to_adjust: (Optional) a different number to apply the calculated translation to

        Returns:
            The translated col_num (or num_to_adjust if provided)

        """
        num_to_adjust = num_to_adjust if num_to_adjust != None else col_num
        return num_to_adjust + (num_columns - 1) - (col_num * 2)


if __name__ == "__main__":
    word_search_parser = WordSearchParser()
    # Skip the first arg because it is our own file name
    word_search_parser.process(sys.argv[1:])
